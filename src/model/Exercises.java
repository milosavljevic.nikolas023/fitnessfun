package model;
// Generated Jan 11, 2019 11:51:24 PM by Hibernate Tools 4.3.1


import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import static javax.persistence.GenerationType.IDENTITY;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;

/**
 * Exercises generated by hbm2java
 */
@Entity
@Table(name="exercises"
    ,catalog="mydb"
)
public class Exercises  implements java.io.Serializable {


     private Integer idExercises;
     private DifficultyLevel difficultyLevel;
     private Users users;
     private String exerciseName;
     private String exerciseDescription;
     private ExercisesHasMuscles exercisesHasMuscles;

    public Exercises() {
    }

	
    public Exercises(DifficultyLevel difficultyLevel, Users users, String exerciseName) {
        this.difficultyLevel = difficultyLevel;
        this.users = users;
        this.exerciseName = exerciseName;
    }
    public Exercises(DifficultyLevel difficultyLevel, Users users, String exerciseName, String exerciseDescription, ExercisesHasMuscles exercisesHasMuscles ) {
       this.difficultyLevel = difficultyLevel;
       this.users = users;
       this.exerciseName = exerciseName;
       this.exerciseDescription = exerciseDescription;
       this.exercisesHasMuscles = exercisesHasMuscles;
    }
   
     @Id @GeneratedValue(strategy=IDENTITY)

    
    @Column(name="idExercises", unique=true, nullable=false)
    public Integer getIdExercises() {
        return this.idExercises;
    }
    
    public void setIdExercises(Integer idExercises) {
        this.idExercises = idExercises;
    }

@ManyToOne(fetch=FetchType.LAZY)
    @JoinColumn(name="difficulty_level_iddifficulty_level", nullable=false)
    public DifficultyLevel getDifficultyLevel() {
        return this.difficultyLevel;
    }
    
    public void setDifficultyLevel(DifficultyLevel difficultyLevel) {
        this.difficultyLevel = difficultyLevel;
    }

@ManyToOne(fetch=FetchType.LAZY)
    @JoinColumn(name="Users_idUsers1", nullable=false)
    public Users getUsers() {
        return this.users;
    }
    
    public void setUsers(Users users) {
        this.users = users;
    }

    
    @Column(name="exercise_name", nullable=false, length=45)
    public String getExerciseName() {
        return this.exerciseName;
    }
    
    public void setExerciseName(String exerciseName) {
        this.exerciseName = exerciseName;
    }

    
    @Column(name="exercise_description", length=45)
    public String getExerciseDescription() {
        return this.exerciseDescription;
    }
    
    public void setExerciseDescription(String exerciseDescription) {
        this.exerciseDescription = exerciseDescription;
    }

@OneToOne(fetch=FetchType.LAZY, mappedBy="exercises")
    public ExercisesHasMuscles getExercisesHasMuscles() {
        return this.exercisesHasMuscles;
    }
    
    public void setExercisesHasMuscles(ExercisesHasMuscles exercisesHasMuscles) {
        this.exercisesHasMuscles = exercisesHasMuscles;
    }



}


