package model;
// Generated Jan 11, 2019 11:51:24 PM by Hibernate Tools 4.3.1


import java.util.HashSet;
import java.util.Set;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import static javax.persistence.GenerationType.IDENTITY;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

/**
 * DifficultyLevel generated by hbm2java
 */
@Entity
@Table(name="difficulty_level"
    ,catalog="mydb"
)
public class DifficultyLevel  implements java.io.Serializable {


     private Integer iddifficultyLevel;
     private String levelsOfDifficulty;
     private Set<Exercises> exerciseses = new HashSet<Exercises>(0);
     private Set<Routines> routineses = new HashSet<Routines>(0);

    public DifficultyLevel() {
    }

    public DifficultyLevel(String levelsOfDifficulty, Set<Exercises> exerciseses, Set<Routines> routineses) {
       this.levelsOfDifficulty = levelsOfDifficulty;
       this.exerciseses = exerciseses;
       this.routineses = routineses;
    }
   
     @Id @GeneratedValue(strategy=IDENTITY)

    
    @Column(name="iddifficulty_level", unique=true, nullable=false)
    public Integer getIddifficultyLevel() {
        return this.iddifficultyLevel;
    }
    
    public void setIddifficultyLevel(Integer iddifficultyLevel) {
        this.iddifficultyLevel = iddifficultyLevel;
    }

    
    @Column(name="levels_of_difficulty", length=45)
    public String getLevelsOfDifficulty() {
        return this.levelsOfDifficulty;
    }
    
    public void setLevelsOfDifficulty(String levelsOfDifficulty) {
        this.levelsOfDifficulty = levelsOfDifficulty;
    }

@OneToMany(fetch=FetchType.LAZY, mappedBy="difficultyLevel")
    public Set<Exercises> getExerciseses() {
        return this.exerciseses;
    }
    
    public void setExerciseses(Set<Exercises> exerciseses) {
        this.exerciseses = exerciseses;
    }

@OneToMany(fetch=FetchType.LAZY, mappedBy="difficultyLevel")
    public Set<Routines> getRoutineses() {
        return this.routineses;
    }
    
    public void setRoutineses(Set<Routines> routineses) {
        this.routineses = routineses;
    }




}


