package model;
// Generated Jan 11, 2019 11:51:24 PM by Hibernate Tools 4.3.1


import java.util.HashSet;
import java.util.Set;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import static javax.persistence.GenerationType.IDENTITY;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

/**
 * Users generated by hbm2java
 */
@Entity
@Table(name="users"
    ,catalog="mydb"
    , uniqueConstraints = @UniqueConstraint(columnNames="username") 
)
public class Users  implements java.io.Serializable {


     private Integer idUsers;
     private String name;
     private String surname;
     private String username;
     private String password;
     private Integer sex;
     private Integer height;
     private UsersHasRoutines usersHasRoutines;
     private Set<ProgressTracker> progressTrackers = new HashSet<ProgressTracker>(0);
     private Set<Exercises> exerciseses = new HashSet<Exercises>(0);

    public Users() {
    }

	
    public Users(String name, String surname, String username, String password, Integer sex) {
        this.name = name;
        this.surname = surname;
        this.username = username;
        this.password = password;
        this.sex = sex;
    }
    public Users(String name, String surname, String username, String password, Integer sex, Integer height, UsersHasRoutines usersHasRoutines, Set<ProgressTracker> progressTrackers, Set<Exercises> exerciseses) {
       this.name = name;
       this.surname = surname;
       this.username = username;
       this.password = password;
       this.sex = sex;
       this.height = height;
       this.usersHasRoutines = usersHasRoutines;
       this.progressTrackers = progressTrackers;
       this.exerciseses = exerciseses;
    }
   
     @Id @GeneratedValue(strategy=IDENTITY)

    
    @Column(name="idUsers", unique=true, nullable=false)
    public Integer getIdUsers() {
        return this.idUsers;
    }
    
    public void setIdUsers(Integer idUsers) {
        this.idUsers = idUsers;
    }

    
    @Column(name="name", nullable=false, length=45)
    public String getName() {
        return this.name;
    }
    
    public void setName(String name) {
        this.name = name;
    }

    
    @Column(name="surname", nullable=false, length=45)
    public String getSurname() {
        return this.surname;
    }
    
    public void setSurname(String surname) {
        this.surname = surname;
    }

    
    @Column(name="username", unique=true, nullable=false, length=45)
    public String getUsername() {
        return this.username;
    }
    
    public void setUsername(String username) {
        this.username = username;
    }

    
    @Column(name="password", nullable=false, length=45)
    public String getPassword() {
        return this.password;
    }
    
    public void setPassword(String password) {
        this.password = password;
    }

    
    @Column(name="sex", nullable=false)
    public Integer getSex() {
        return this.sex;
    }
    
    public void setSex(Integer sex) {
        this.sex = sex;
    }

    
    @Column(name="height")
    public Integer getHeight() {
        return this.height;
    }
    
    public void setHeight(Integer height) {
        this.height = height;
    }

@OneToOne(fetch=FetchType.LAZY, mappedBy="users")
    public UsersHasRoutines getUsersHasRoutines() {
        return this.usersHasRoutines;
    }
    
    public void setUsersHasRoutines(UsersHasRoutines usersHasRoutines) {
        this.usersHasRoutines = usersHasRoutines;
    }

@OneToMany(fetch=FetchType.LAZY, mappedBy="users")
    public Set<ProgressTracker> getProgressTrackers() {
        return this.progressTrackers;
    }
    
    public void setProgressTrackers(Set<ProgressTracker> progressTrackers) {
        this.progressTrackers = progressTrackers;
    }

@OneToMany(fetch=FetchType.LAZY, mappedBy="users")
    public Set<Exercises> getExerciseses() {
        return this.exerciseses;
    }
    
    public void setExerciseses(Set<Exercises> exerciseses) {
        this.exerciseses = exerciseses;
    }

    @Override
    public String toString() {
        return "Users{" + "idUsers=" + idUsers + ", name=" + name + ", surname=" + surname + ", username=" + username + ", password=" + password + ", sex=" + sex + ", height=" + height + ", usersHasRoutines=" + usersHasRoutines + ", progressTrackers=" + progressTrackers + ", exerciseses=" + exerciseses + '}';
    }




}


