package model;
// Generated Jan 11, 2019 11:51:24 PM by Hibernate Tools 4.3.1


import java.util.Date;
import java.util.HashSet;
import java.util.Set;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import static javax.persistence.GenerationType.IDENTITY;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * ProgressTracker generated by hbm2java
 */
@Entity
@Table(name="progress_tracker"
    ,catalog="mydb"
)
public class ProgressTracker  implements java.io.Serializable {


     private Integer idStats;
     private Users users;
     private Integer weight;
     private Integer exerciseRepsTracker;
     private Date date;

    public ProgressTracker() {
    }

	
    public ProgressTracker(Users users) {
        this.users = users;
    }
    public ProgressTracker(Users users, Integer weight, Integer exerciseRepsTracker, Date date) {
       this.users = users;
       this.weight = weight;
       this.exerciseRepsTracker = exerciseRepsTracker;
       this.date = date;
    }
   
     @Id @GeneratedValue(strategy=IDENTITY)

    
    @Column(name="idStats", unique=true, nullable=false)
    public Integer getIdStats() {
        return this.idStats;
    }
    
    public void setIdStats(Integer idStats) {
        this.idStats = idStats;
    }

@ManyToOne(fetch=FetchType.LAZY)
    @JoinColumn(name="Users_idUsers", nullable=false)
    public Users getUsers() {
        return this.users;
    }
    
    public void setUsers(Users users) {
        this.users = users;
    }

    
    @Column(name="weight")
    public Integer getWeight() {
        return this.weight;
    }
    
    public void setWeight(Integer weight) {
        this.weight = weight;
    }

    
    @Column(name="exercise_reps_tracker")
    public Integer getExerciseRepsTracker() {
        return this.exerciseRepsTracker;
    }
    
    public void setExerciseRepsTracker(Integer exerciseRepsTracker) {
        this.exerciseRepsTracker = exerciseRepsTracker;
    }

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name="date", length=19)
    public Date getDate() {
        return this.date;
    }
    
    public void setDate(Date date) {
        this.date = date;
    }







}


