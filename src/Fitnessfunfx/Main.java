/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Fitnessfunfx;

import java.io.IOException;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;
import org.hibernate.SessionFactory;
import utility.HibernateUtil;

/**
 *
 * @author Nikolas007
 */
public class Main extends Application {
    
    @Override
    public void start(Stage primaryStage) throws IOException {
        SessionFactory sessionFactory = HibernateUtil.getSessionFactory();
    System.out.println("Session Factory : " + sessionFactory.hashCode());
        Parent root = FXMLLoader.load(getClass().getResource("/Fitnessfunfx/Main.fxml"));
        Scene scene = new Scene(root,1005,639);
        scene.getStylesheets().add(getClass().getResource("application.css").toExternalForm());
        primaryStage.setScene(scene);
        
        primaryStage.setResizable(false);
        primaryStage.show();
        primaryStage.setOnCloseRequest(e -> {
            closeProgram();
        });
        
    }

    public static void main(String[] args) {
        launch(args);
    }
    public void closeProgram() {
        System.exit(0);
    }
    
}
