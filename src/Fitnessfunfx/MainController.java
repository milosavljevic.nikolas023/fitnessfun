/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Fitnessfunfx;

import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXPasswordField;
import com.jfoenix.controls.JFXRadioButton;
import com.jfoenix.controls.JFXTextArea;
import com.jfoenix.controls.JFXTextField;
import java.net.URL;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.Pane;
import java.util.List;
import javafx.scene.chart.CategoryAxis;
import javafx.scene.chart.LineChart;
import javafx.scene.chart.NumberAxis;
import javafx.scene.chart.XYChart;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.Label;
import javafx.scene.input.MouseEvent;
import model.DifficultyLevel;
import model.Exercises;
import model.ExercisesHasMuscles;
import model.Muscles;
import model.ProgressTracker;
import model.Routines;
import model.Users;
import model.UsersHasRoutines;
import org.hibernate.Query;
import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.hibernate.engine.jdbc.spi.SqlExceptionHelper;
import utility.HibernateUtil;

/**
 * FXML Controller class
 *
 * @author Nikolas007
 */
public class MainController implements Initializable {

    URL url;
    ResourceBundle rb;

    @FXML
    private Pane pnl_current_workout, pnl_all_workouts,
            pnl_all_exercises, pnl_login, pnl_registration, pnl_empty_field,
            pnl_log_reg, pnl_logged_in, pnl_admin, pnl_progress_tracker, pnl_chosen_routine, pnl_chose_routine,
            pnl_empty_field1;

    @FXML
    private JFXButton btn_current_workout, btn_all_workouts,
            btn_all_exercises, btn_login, btn_registration,
            btn_reg_create, btn_empty_field, btn_login_check, btn_log_out,
            btn_progress_tracker, btn_insert_diff, btn_delete_diff, btn_insert_workout,
            btn_delete_workout, btn_insert_exercise, btn_delete_exercise, btn_delete_user,
            btn_insert_muscle, btn_delete_muscle, btn_insert_weight, btn_update, btn_chose_workout,
            btn_user_work, btn_user_workouts, btn_user_exercises, btn_empty_field1;

    @FXML
    private Label lbl_user_exe, lbl_welcome_user, lbl_user_workName, lbl_user_workGoal,
            lbl_user_workLvl, lbl_user_workDesc,lbl_user_diffE,lbl_user_nameE,
            lbl_user_muscleE,lbl_cur_name,lbl_cur_goal,lbl_cur_des,lbl_cur_lvl;

    @FXML
    private ChoiceBox<String> cb_diff = new ChoiceBox<>();

    @FXML
    private ChoiceBox<String> cb_exercises = new ChoiceBox<>();
    @FXML
    private ChoiceBox<String> cb_exe_muscles = new ChoiceBox<>();

    @FXML
    private ChoiceBox<String> cb_diff_exe = new ChoiceBox<>();
    @FXML
    private ChoiceBox<String> cb_diff_work = new ChoiceBox<>();
    @FXML
    private ChoiceBox<String> cb_admin_users = new ChoiceBox<>();
    @FXML
    private ChoiceBox<String> cb_muscles = new ChoiceBox<>();
    @FXML
    private ChoiceBox<String> cb_chose_workout = new ChoiceBox<>();

    @FXML
    private ChoiceBox<String> cb_workouts = new ChoiceBox<>();
    @FXML
    private ChoiceBox<String> cb_users = new ChoiceBox<>();
    @FXML
    private ChoiceBox<String> cb_user_diff = new ChoiceBox<>();
    @FXML
    private ChoiceBox<String> cb_user_workouts = new ChoiceBox<>();

    @FXML
    private ChoiceBox<String> cb_user_exercises = new ChoiceBox<>();

    @FXML
    private ChoiceBox<String> cb_user_musc = new ChoiceBox<>();

    @FXML
    private JFXTextField txt_username, txt_reg_height, txt_reg_surname, txt_reg_name, txt_reg_username,
            txt_diff_lvl, txt_routine_name, txt_routine_goal, txt_exercise_name, txt_muscles, txt_current_weight,
            txt_cur_name, txt_cur_des, txt_cur_goal, txt_cur_lvl, txt_user_name, txt_user_goal, txt_user_workName,
            txt_user_workGoal, txt_user_workLvl, txt_user_nameE, txt_user_diffE;

    @FXML
    private JFXTextArea txt_routine_description, txt_exercise_description, ta_user_desc, ta_user_workDesc,
            ta_user_desE;

    @FXML
    private JFXRadioButton rb_reg_male;
    @FXML
    private JFXRadioButton rb_reg_female;

    @FXML
    private JFXPasswordField pf_password, pf_reg_password;

    @FXML
    private AnchorPane acp_workout, acp_workouts,
            acp_exercises, acp_main, acp_login, acp_progress_tracker;
    @FXML
    private CategoryAxis x;
    @FXML
    private NumberAxis y;

    @FXML
    private LineChart<?, ?> lc_wot2;

    @FXML
    private void buttonActionHandler(ActionEvent event) {
        if (event.getSource() == btn_registration) {
            pnl_registration.toFront();
        }
        if (event.getSource() == btn_login) {
            pnl_login.toFront();
        }

        //REGISTRATION--------------------------------------------------------------------------------------------------
        if (event.getSource() == btn_reg_create) {
            Users p = new Users();
            p.setName(txt_reg_name.getText());
            p.setUsername(txt_reg_username.getText());
            p.setSurname(txt_reg_surname.getText());
            p.setPassword(pf_reg_password.getText());

            try {
                p.setHeight(Integer.parseInt(txt_reg_height.getText()));
            } catch (NumberFormatException e) {
                pnl_empty_field.toFront();
                return;
            }

            if (rb_reg_male.isSelected()) {
                System.out.println("MALE____________________________");
                p.setSex(0);
            } else if (rb_reg_female.isSelected()) {
                System.out.println("FEMALE========================");
                p.setSex(1);
            } else {
                p.setSex(2);
            }
            System.out.println(p);
            if (p.getName().length() == 0 || p.getSurname().length() == 0 || p.getPassword().length() == 0 || p.getUsername().length() == 0 || p.getSex() == 2) {
                pnl_empty_field.toFront();

            } else if (p.getUsername().length() < 8) {
                pnl_empty_field1.toFront();
            } else {
                Session sesija = HibernateUtil.getSessionFactory().openSession();
                sesija.beginTransaction();

                sesija.save(p);
                sesija.getTransaction().commit();
                sesija.close();

                pnl_login.toFront();
                acp_login.toFront();
            }

        }

        //LOGIN---------------------------------------------------------------------------------------------------------
        if (event.getSource() == btn_login_check) {

            Session sesija = HibernateUtil.getSessionFactory().openSession();
            sesija.beginTransaction();
            Query q = sesija.createQuery("from Users where username = :username and password = :password");
            q.setParameter("username", txt_username.getText());
            q.setParameter("password", pf_password.getText());
            List<Users> g = q.list();
            if (txt_username.getText().equals("admin") && pf_password.getText().equals("admin")) {
                pnl_admin.toFront();
                pnl_logged_in.toBack();
                pnl_log_reg.toBack();
            } else if (g.size() > 0) {
                System.out.println("ULOGOVAN");
                pnl_logged_in.toFront();
                pnl_progress_tracker.toFront();
                acp_progress_tracker.toFront();
                lbl_welcome_user.setText(txt_username.getText());
                sesija.getTransaction().commit();
                sesija.close();

                sesija = HibernateUtil.getSessionFactory().openSession();
                sesija.beginTransaction();

                q = sesija.createQuery("from Users where username= :username");
                q.setParameter("username", txt_username.getText());
                int broj = ((Users) q.list().get(0)).getIdUsers();

                sesija.getTransaction().commit();
                sesija.close();

                sesija = HibernateUtil.getSessionFactory().openSession();
                sesija.beginTransaction();
                ArrayList<UsersHasRoutines> lista;
                q = sesija.createQuery("from UsersHasRoutines");
                lista = (ArrayList<UsersHasRoutines>) q.list();
                sesija.getTransaction().commit();
                sesija.close();
                System.out.println("9999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999");
                int provera = 0;
                for (UsersHasRoutines s : lista) {
                    if (s.getUsers().getIdUsers() == broj) {
                        provera++;
                    }
                }
                System.out.println("''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''" + provera);
                System.out.println("");
                if (provera == 0) {
                    pnl_chose_routine.toFront();
                    pnl_chosen_routine.toBack();
                } else {

                    sesija = HibernateUtil.getSessionFactory().openSession();
                    sesija.beginTransaction();
                    q = sesija.createQuery("from UsersHasRoutines");

                    int broj1 = ((UsersHasRoutines) q.list().get(0)).getRoutines().getIdRoutines();
                    System.out.println(broj1);
                    sesija.getTransaction().commit();
                    sesija.close();

                    sesija = HibernateUtil.getSessionFactory().openSession();
                    sesija.beginTransaction();
                    q = sesija.createQuery("from Routines where idRoutines= :nesto");
                    q.setParameter("nesto", broj1);
                    String ime = ((Routines) q.list().get(0)).getRoutineName();
                    DifficultyLevel lvl = ((Routines) q.list().get(0)).getDifficultyLevel();
                    String goal = ((Routines) q.list().get(0)).getRoutineGoal();
                    String des = ((Routines) q.list().get(0)).getRoutineDescription();
                    pnl_chose_routine.toBack();
                    pnl_chosen_routine.toFront();
                    lbl_cur_name.setText(ime);
                    lbl_cur_goal.setText(goal);
                    lbl_cur_lvl.setText(lvl.getLevelsOfDifficulty());
                    lbl_cur_des.setText(des);
                    System.out.println(ime + "  " + goal + "  " + des + "  " + lvl.getLevelsOfDifficulty());
                    sesija.getTransaction().commit();
                    sesija.close();

                }

            } else {
                System.out.println("Nije Ulogovan");

            }
            // POSTAVLJANJE COMBO BOXA DIFF LVL
            //Postavljanje u diff_lvl choice box -----------------------------------------------------------------------------------------
            ArrayList<DifficultyLevel> diff_lvl;

            sesija = HibernateUtil.getSessionFactory().openSession();
            sesija.beginTransaction();
            q = sesija.createQuery("from DifficultyLevel");
            diff_lvl = (ArrayList<DifficultyLevel>) q.list();

            sesija.getTransaction().commit();
            sesija.close();

            cb_user_diff.getItems().clear();

            for (DifficultyLevel s : diff_lvl) {
                cb_user_diff.getItems().add(s.getLevelsOfDifficulty());

            }
        }

//<editor-fold>
        //ADMIN PAGE ------------------------------------------------------------------------------------------------
        //UNOS NOVOG TRENINGA ------------------------------------------------------------------------------------------------
        if (event.getSource()
                == btn_insert_workout) {

            Session sesija = HibernateUtil.getSessionFactory().openSession();
            sesija.beginTransaction();

            Query q = sesija.createQuery("from DifficultyLevel where levelsOfDifficulty= :diff");
            q.setParameter("diff", cb_diff_work.getValue());
            int broj = ((DifficultyLevel) q.list().get(0)).getIddifficultyLevel().intValue();

            sesija.getTransaction().commit();
            sesija.close();

            Routines r = new Routines();
            r.setRoutineName(txt_routine_name.getText());
            r.setRoutineGoal(txt_routine_goal.getText());
            r.setRoutineDescription(txt_routine_description.getText());

            DifficultyLevel d = new DifficultyLevel();
            d.setIddifficultyLevel(broj);
            r.setDifficultyLevel(d);
            cb_diff_work.getValue();

            sesija = HibernateUtil.getSessionFactory().openSession();
            sesija.beginTransaction();

            sesija.save(r);
            sesija.getTransaction().commit();
            sesija.close();

            //Rifresovanje comboBoxa  RUTINE-------------------------------------------------------------
            ArrayList<Routines> routines;
            sesija = HibernateUtil.getSessionFactory().openSession();
            sesija.beginTransaction();
            q = sesija.createQuery("from Routines");
            routines = (ArrayList<Routines>) q.list();
            sesija.getTransaction().commit();
            sesija.close();

            cb_workouts.getItems().clear();
            for (Routines s : routines) {
                cb_workouts.getItems().add(s.getRoutineName());
            }
        }
        //UNOS NOVE VEZBE --------------------------------------------------------------------------------------------

        if (event.getSource() == btn_insert_exercise) {

            List<DifficultyLevel> diff_lvls = null;

            Session sesija = HibernateUtil.getSessionFactory().openSession();
            sesija.beginTransaction();

            Query q = sesija.createQuery("from DifficultyLevel where levelsOfDifficulty= :diff");

            q.setParameter("diff", cb_diff_exe.getValue());

            int broj = ((DifficultyLevel) q.list().get(0)).getIddifficultyLevel().intValue();

            sesija.getTransaction().commit();
            sesija.close();

            sesija = HibernateUtil.getSessionFactory().openSession();
            sesija.beginTransaction();
            Query y = sesija.createQuery("from Users where username= :username");
            y.setParameter("username", cb_users.getValue());
            int broj1 = ((Users) y.list().get(0)).getIdUsers().intValue();
            sesija.getTransaction().commit();
            sesija.close();

            Exercises r = new Exercises();
            r.setExerciseName(txt_exercise_name.getText());
            r.setExerciseDescription(txt_exercise_description.getText());

            DifficultyLevel d = new DifficultyLevel();
            d.setIddifficultyLevel(broj);
            r.setDifficultyLevel(d);

            Users u = new Users();
            u.setIdUsers(broj1);
            r.setUsers(u);
            cb_diff_exe.getValue();

            sesija = HibernateUtil.getSessionFactory().openSession();
            sesija.beginTransaction();

            sesija.save(r);
            sesija.getTransaction().commit();
            sesija.close();

            sesija = HibernateUtil.getSessionFactory().openSession();
            sesija.beginTransaction();
            y = sesija.createQuery("from Muscles where muscleName= :username");
            y.setParameter("username", cb_exe_muscles.getValue());
            int broj3 = ((Muscles) y.list().get(0)).getIdMusclesParts();
            sesija.getTransaction().commit();
            sesija.close();

            Muscles m = new Muscles();
            m.setIdMusclesParts(broj3);

            ExercisesHasMuscles ehm = new ExercisesHasMuscles();
            ehm.setExercises(r);
            ehm.setMuscles(m);

            sesija = HibernateUtil.getSessionFactory().openSession();
            sesija.beginTransaction();
            sesija.save(ehm);
            sesija.getTransaction().commit();
            sesija.close();

            ArrayList<Exercises> exercises;
            sesija = HibernateUtil.getSessionFactory().openSession();
            sesija.beginTransaction();
            q = sesija.createQuery("from Exercises");
            exercises = (ArrayList<Exercises>) q.list();
            sesija.getTransaction().commit();
            sesija.close();

            cb_exercises.getItems().clear();
            for (Exercises s : exercises) {
                cb_exercises.getItems().add(s.getExerciseName());
            }

            //Rifresovanje comboBoxa  RUTINE-------------------------------------------------------------
            ArrayList<Routines> routines;
            sesija = HibernateUtil.getSessionFactory().openSession();
            sesija.beginTransaction();
            q = sesija.createQuery("from Routines");
            routines = (ArrayList<Routines>) q.list();
            sesija.getTransaction().commit();
            sesija.close();

            cb_workouts.getItems().clear();
            for (Routines s : routines) {
                cb_workouts.getItems().add(s.getRoutineName());
            }
        }

        //BRISANJE TRENINGA -----------------------------------------------------------------------------------------
        if (event.getSource()
                == btn_delete_workout) {
            Session sesija = HibernateUtil.getSessionFactory().openSession();
            sesija.beginTransaction();

            Query q = sesija.createQuery("delete from Routines where routineName= :name");
            q.setParameter("name", cb_workouts.getValue());
            q.executeUpdate();
            sesija.getTransaction().commit();
            sesija.close();

            //rifresovanje choiceBox-a -------------------------------------------
            ArrayList<Routines> routines;
            sesija = HibernateUtil.getSessionFactory().openSession();
            sesija.beginTransaction();
            q = sesija.createQuery("from Routines");
            routines = (ArrayList<Routines>) q.list();
            sesija.getTransaction().commit();
            sesija.close();

            cb_workouts.getItems().clear();
            for (Routines s : routines) {
                cb_workouts.getItems().add(s.getRoutineName());
            }
        }
        //BRISANJE VEZBE -------------------------------------------------------------------------------------

        if (event.getSource()
                == btn_delete_exercise) {
            Session sesija = HibernateUtil.getSessionFactory().openSession();
            sesija.beginTransaction();
            Query q = sesija.createQuery("from Exercises where exerciseName= :names");
            q.setParameter("names", cb_exercises.getValue());
            int broj1 = ((Exercises) q.list().get(0)).getIdExercises();
            sesija.getTransaction().commit();
            sesija.close();

            sesija = HibernateUtil.getSessionFactory().openSession();
            sesija.beginTransaction();
            SQLQuery m = sesija.createSQLQuery("delete from Exercises_Has_Muscles where Exercises_idExercises = :names");
            m.addEntity(ExercisesHasMuscles.class);
            m.setParameter("names", broj1);
            m.executeUpdate();
            sesija.getTransaction().commit();
            sesija.close();

            sesija = HibernateUtil.getSessionFactory().openSession();
            sesija.beginTransaction();

            q = sesija.createQuery("delete from Exercises where exerciseName= :names");
            q.setParameter("names", cb_exercises.getValue());
            q.executeUpdate();
            sesija.getTransaction().commit();
            sesija.close();

            //rifresovanje choiceBox-a -------------------------------------------
            ArrayList<Exercises> exercise;
            sesija = HibernateUtil.getSessionFactory().openSession();
            sesija.beginTransaction();
            q = sesija.createQuery("from Exercises");
            exercise = (ArrayList<Exercises>) q.list();
            sesija.getTransaction().commit();
            sesija.close();

            cb_exercises.getItems().clear();
            for (Exercises s : exercise) {
                cb_exercises.getItems().add(s.getExerciseName());
            }
        }
        //BRISANJE KORISNIKA -----------------------------------------------------------------------------------------

        if (event.getSource()
                == btn_delete_user) {
            Session sesija = HibernateUtil.getSessionFactory().openSession();
            sesija.beginTransaction();

            Query q = sesija.createQuery("delete from Users where username= :ime");
            q.setParameter("ime", cb_admin_users.getValue());
            q.executeUpdate();
            sesija.getTransaction().commit();
            sesija.close();

            //Resetovanje Users choice box-a -----------------------------------------------------------------------
            ArrayList<Users> users;
            sesija = HibernateUtil.getSessionFactory().openSession();
            sesija.beginTransaction();
            q = sesija.createQuery("from Users");
            users = (ArrayList<Users>) q.list();
            sesija.getTransaction().commit();
            sesija.close();

            cb_admin_users.getItems().clear();
            for (Users s : users) {
                cb_admin_users.getItems().add(s.getUsername());
            }

            cb_users.getItems().clear();
            for (Users s : users) {
                cb_users.getItems().add(s.getUsername());
            }
        }

        //UNOS DIFF LVL-a -----------------------------------------------------------------------------------------
        if (event.getSource()
                == btn_insert_diff) {

            DifficultyLevel z = new DifficultyLevel();
            z.setLevelsOfDifficulty(txt_diff_lvl.getText());

            Session sesija = HibernateUtil.getSessionFactory().openSession();
            sesija.beginTransaction();

            sesija.save(z);
            sesija.getTransaction().commit();
            sesija.close();

            //Postavljanje u diff_lvl choice box -----------------------------------------------------------------------------------------
            ArrayList<DifficultyLevel> diff_lvl;

            sesija = HibernateUtil.getSessionFactory().openSession();
            sesija.beginTransaction();
            Query q = sesija.createQuery("from DifficultyLevel");
            diff_lvl = (ArrayList<DifficultyLevel>) q.list();

            sesija.getTransaction().commit();
            sesija.close();

            cb_diff.getItems().clear();
            cb_diff_exe.getItems().clear();
            cb_diff_work.getItems().clear();
            cb_user_diff.getItems().clear();

            for (DifficultyLevel s : diff_lvl) {
                cb_diff.getItems().add(s.getLevelsOfDifficulty());
                cb_diff_exe.getItems().add(s.getLevelsOfDifficulty());
                cb_diff_work.getItems().add(s.getLevelsOfDifficulty());
                cb_user_diff.getItems().add(s.getLevelsOfDifficulty());

            }

        }
        //BRISANJE DIFF LVL-a -----------------------------------------------------------------------------------------

        if (event.getSource()
                == btn_delete_diff) {

            Session sesija = HibernateUtil.getSessionFactory().openSession();
            sesija.beginTransaction();

            Query q = sesija.createQuery("delete from DifficultyLevel where levelsOfDifficulty= :lvl");
            q.setParameter("lvl", cb_diff.getValue());
            q.executeUpdate();
            sesija.getTransaction().commit();
            sesija.close();

            //Postavljanje u diff_lvl choice box -----------------------------------------------------------------------------------------
            ArrayList<DifficultyLevel> diff_lvl;

            sesija = HibernateUtil.getSessionFactory().openSession();
            sesija.beginTransaction();
            q = sesija.createQuery("from DifficultyLevel");
            diff_lvl = (ArrayList<DifficultyLevel>) q.list();

            sesija.getTransaction().commit();
            sesija.close();

            cb_diff.getItems().clear();
            cb_diff_exe.getItems().clear();
            cb_diff_work.getItems().clear();

            for (DifficultyLevel s : diff_lvl) {
                cb_diff.getItems().add(s.getLevelsOfDifficulty());
                cb_diff_exe.getItems().add(s.getLevelsOfDifficulty());
                cb_diff_work.getItems().add(s.getLevelsOfDifficulty());
            }

        }
        //UNOS MISICA ----------------------------------------------------------------------------------------------------------------------------------------------------------

        if (event.getSource()
                == btn_insert_muscle) {

            Muscles z = new Muscles();
            z.setMuscleName(txt_muscles.getText());

            Session sesija = HibernateUtil.getSessionFactory().openSession();
            sesija.beginTransaction();

            sesija.save(z);
            sesija.getTransaction().commit();
            sesija.close();
            //Resetovanje MUSCLES choice boxa ------------------------------------------------------------------------------------------------------------------------------------

            ArrayList<Muscles> muscles;
            sesija = HibernateUtil.getSessionFactory().openSession();
            sesija.beginTransaction();
            Query q = sesija.createQuery("from Muscles");
            muscles = (ArrayList<Muscles>) q.list();

            sesija.getTransaction().commit();
            sesija.close();

            cb_muscles.getItems().clear();
            cb_exe_muscles.getItems().clear();

            for (Muscles s : muscles) {
                cb_muscles.getItems().add(s.getMuscleName());
                cb_exe_muscles.getItems().add(s.getMuscleName());
            }

        }

        //BRISANJE MISICA ----------------------------------------------------------------------------------------------------------------------------------------------------------
        if (event.getSource()
                == btn_delete_muscle) {
            Session sesija = HibernateUtil.getSessionFactory().openSession();
            sesija.beginTransaction();

            Query q = sesija.createQuery("delete from Muscles where muscleName= :misic");
            q.setParameter("misic", cb_muscles.getValue());
            q.executeUpdate();
            sesija.getTransaction().commit();
            sesija.close();

            ArrayList<Muscles> muscles;
            sesija = HibernateUtil.getSessionFactory().openSession();
            sesija.beginTransaction();
            q = sesija.createQuery("from Muscles");
            muscles = (ArrayList<Muscles>) q.list();

            sesija.getTransaction().commit();
            sesija.close();

            cb_muscles.getItems().clear();

            for (Muscles s : muscles) {
                cb_muscles.getItems().add(s.getMuscleName());
            }
        }
        //</editor-fold>

        if (event.getSource()
                == btn_insert_weight) {

            Users users = new Users();
            Date date = new Date();

            List<DifficultyLevel> diff_lvls = null;

            Session sesija = HibernateUtil.getSessionFactory().openSession();
            sesija.beginTransaction();

            Query q = sesija.createQuery("from Users where username= :username");
            q.setParameter("username", txt_username.getText());
            int broj1 = ((Users) q.list().get(0)).getIdUsers().intValue();

            sesija.getTransaction().commit();
            sesija.close();
            users.setIdUsers(broj1);
            ProgressTracker pt = new ProgressTracker();
            pt.setWeight(Integer.parseInt(txt_current_weight.getText()));
            pt.setUsers(users);
            pt.setDate(date);

            sesija = HibernateUtil.getSessionFactory().openSession();
            sesija.beginTransaction();
            sesija.save(pt);
            sesija.getTransaction().commit();
            sesija.close();

        }
        //UPDATE CHART ------------------------------------------------------------------------------------------------------------------------------------------------------

        if (event.getSource() == btn_update) {

            Session sesija = HibernateUtil.getSessionFactory().openSession();
            sesija.beginTransaction();

            Query q = sesija.createQuery("from Users where username= :username");
            q.setParameter("username", txt_username.getText());
            int broj = ((Users) q.list().get(0)).getIdUsers();

            sesija.getTransaction().commit();
            sesija.close();

            Users user = new Users();
            user.setIdUsers(broj);

            ArrayList<ProgressTracker> tracker;

            sesija = HibernateUtil.getSessionFactory().openSession();
            sesija.beginTransaction();
            q = sesija.createQuery("from ProgressTracker where users= :users");
            q.setParameter("users", user);
            tracker = (ArrayList<ProgressTracker>) q.list();

            sesija.getTransaction().commit();
            sesija.close();

            lc_wot2.getData().clear();
            XYChart.Series series = new XYChart.Series();

            for (ProgressTracker s : tracker) {
                series.getData().add(new XYChart.Data(s.getDate().toString(), s.getWeight()));

            }

            lc_wot2.getData().addAll(series);

        }

        //CHOSE WORKOUT -------------------------------------------------------------------------------------------------
        if (event.getSource()
                == btn_chose_workout) {

            Session sesija = HibernateUtil.getSessionFactory().openSession();
            sesija.beginTransaction();

            Query q = sesija.createQuery("from Users where username= :username");
            q.setParameter("username", txt_username.getText());
            int broj = ((Users) q.list().get(0)).getIdUsers();

            sesija.getTransaction().commit();
            sesija.close();

            sesija = HibernateUtil.getSessionFactory().openSession();
            sesija.beginTransaction();

            q = sesija.createQuery("from Routines where routineName= :username");
            q.setParameter("username", cb_chose_workout.getValue());
            int broj2 = ((Routines) q.list().get(0)).getIdRoutines();

            sesija.getTransaction().commit();
            sesija.close();

            UsersHasRoutines uhr = new UsersHasRoutines();

            Users u = new Users();
            u.setIdUsers(broj);

            Routines r = new Routines();
            r.setIdRoutines(broj2);

            uhr.setUsers(u);
            uhr.setRoutines(r);
            System.out.println(uhr);
            sesija = HibernateUtil.getSessionFactory().openSession();
            sesija.beginTransaction();
            sesija.save(uhr);
            sesija.getTransaction().commit();
            sesija.close();

            //Postavljanje workout choice box-a 
            ArrayList<Routines> routines;
            sesija = HibernateUtil.getSessionFactory().openSession();
            sesija.beginTransaction();
            q = sesija.createQuery("from Routines");
            routines = (ArrayList<Routines>) q.list();
            sesija.getTransaction().commit();
            sesija.close();

            cb_workouts.getItems().clear();
            for (Routines s : routines) {
                cb_workouts.getItems().add(s.getRoutineName());
            }

            sesija = HibernateUtil.getSessionFactory().openSession();
            sesija.beginTransaction();
            q = sesija.createQuery("from UsersHasRoutines");

            int broj3 = ((UsersHasRoutines) q.list().get(0)).getRoutines().getIdRoutines();
            System.out.println(broj3);
            sesija.getTransaction().commit();
            sesija.close();

            sesija = HibernateUtil.getSessionFactory().openSession();
            sesija.beginTransaction();
            q = sesija.createQuery("from Routines where idRoutines= :nesto");
            q.setParameter("nesto", broj3);
            String ime = ((Routines) q.list().get(0)).getRoutineName();
            DifficultyLevel lvl = ((Routines) q.list().get(0)).getDifficultyLevel();
            String goal = ((Routines) q.list().get(0)).getRoutineGoal();
            String des = ((Routines) q.list().get(0)).getRoutineDescription();
            pnl_chose_routine.toBack();
            pnl_chosen_routine.toFront();
            lbl_cur_name.setText(ime);
            lbl_cur_goal.setText(goal);
            lbl_cur_lvl.setText(lvl.getLevelsOfDifficulty());
            lbl_cur_des.setText(des);
            System.out.println(ime + "  " + goal + "  " + des + "  " + lvl.getLevelsOfDifficulty());
            sesija.getTransaction().commit();
            sesija.close();

            pnl_chosen_routine.toFront();

        }
        //PRIKAZ TRENINGA U ALL WORKOUTS TABU ---------------------------------------------------------------------------------------------------------

        if (event.getSource()
                == btn_user_workouts) {
            Session sesija = HibernateUtil.getSessionFactory().openSession();
            sesija.beginTransaction();
            Query q = sesija.createQuery("from Routines where routineName= :nesto");
            q.setParameter("nesto", cb_user_workouts.getValue());
            String ime = ((Routines) q.list().get(0)).getRoutineName();
            DifficultyLevel lvl = ((Routines) q.list().get(0)).getDifficultyLevel();
            String goal = ((Routines) q.list().get(0)).getRoutineGoal();
            String des = ((Routines) q.list().get(0)).getRoutineDescription();

            lbl_user_workLvl.setText(lvl.getLevelsOfDifficulty());
            lbl_user_workDesc.setText(des);
            lbl_user_workName.setText(ime);
            lbl_user_workGoal.setText(goal);
            sesija.getTransaction().commit();
            sesija.close();
        }
        //PRIKAZ VEZBI U ALL EXERCISES TABU -------------------------------------------------------------------------------------------------------

        if (event.getSource()
                == btn_user_exercises) {
            Session sesija = HibernateUtil.getSessionFactory().openSession();
            sesija.beginTransaction();
            Query q = sesija.createQuery("from Exercises where exerciseName= :nesto");
            q.setParameter("nesto", cb_user_exercises.getValue());
            int broj = ((Exercises) q.list().get(0)).getIdExercises();
            String ime = ((Exercises) q.list().get(0)).getExerciseName();
            DifficultyLevel lvl = ((Exercises) q.list().get(0)).getDifficultyLevel();
            String des = ((Exercises) q.list().get(0)).getExerciseDescription();
            lbl_user_nameE.setText(ime);
            lbl_user_diffE.setText(lvl.getLevelsOfDifficulty());
            sesija.getTransaction().commit();
            sesija.close();
            
            
            lbl_user_muscleE.setText(des);
            
            

            ArrayList<ExercisesHasMuscles> list;
            sesija = HibernateUtil.getSessionFactory().openSession();
            sesija.beginTransaction();
            q = sesija.createQuery("from ExercisesHasMuscles");
            list = (ArrayList<ExercisesHasMuscles>) q.list();
            sesija.getTransaction().commit();
            sesija.close();
            for (ExercisesHasMuscles s : list) {
                if (s.getExercises().getIdExercises() == broj) {
                    int broj3 = s.getMuscles().getIdMusclesParts();
                    sesija = HibernateUtil.getSessionFactory().openSession();
                    sesija.beginTransaction();
                    q = sesija.createQuery("from Muscles where idMusclesParts= :nesto");
                    q.setParameter("nesto", broj3);
                    lbl_user_exe.setText(((Muscles) q.list().get(0)).getMuscleName());
                    sesija.getTransaction().commit();
                    sesija.close();
                }
            }

        }

        //KORISNIK UNOSI WORKOUT ----------------------------------------------------------------------------------------------------------------
        if (event.getSource()
                == btn_user_work) {
            Session sesija = HibernateUtil.getSessionFactory().openSession();
            sesija.beginTransaction();

            Query q = sesija.createQuery("from DifficultyLevel where levelsOfDifficulty= :diff");
            q.setParameter("diff", cb_user_diff.getValue());
            int broj = ((DifficultyLevel) q.list().get(0)).getIddifficultyLevel().intValue();

            sesija.getTransaction().commit();
            sesija.close();

            Routines r = new Routines();
            r.setRoutineName(txt_user_name.getText());
            r.setRoutineGoal(txt_user_goal.getText());
            r.setRoutineDescription(ta_user_desc.getText());

            DifficultyLevel d = new DifficultyLevel();
            d.setIddifficultyLevel(broj);
            r.setDifficultyLevel(d);
            sesija = HibernateUtil.getSessionFactory().openSession();
            sesija.beginTransaction();

            sesija.save(r);
            sesija.getTransaction().commit();
            sesija.close();
            ArrayList<Exercises> exercises;
            sesija = HibernateUtil.getSessionFactory().openSession();
            sesija.beginTransaction();
            q = sesija.createQuery("from Exercises");
            exercises = (ArrayList<Exercises>) q.list();
            sesija.getTransaction().commit();
            sesija.close();

            cb_exercises.getItems().clear();
            cb_user_exercises.getItems().clear();
            for (Exercises s : exercises) {
                cb_exercises.getItems().add(s.getExerciseName());
                cb_user_exercises.getItems().add(s.getExerciseName());
            }

            //Postavljanje workout choice box-a 
            ArrayList<Routines> routines;
            sesija = HibernateUtil.getSessionFactory().openSession();
            sesija.beginTransaction();
            q = sesija.createQuery("from Routines");
            routines = (ArrayList<Routines>) q.list();
            sesija.getTransaction().commit();
            sesija.close();

            cb_workouts.getItems().clear();
            for (Routines s : routines) {
                cb_workouts.getItems().add(s.getRoutineName());
            }
            //Postavljanje workout choice box-a 
            ArrayList<Routines> routines1;
            sesija = HibernateUtil.getSessionFactory().openSession();
            sesija.beginTransaction();
            q = sesija.createQuery("from Routines");
            routines1 = (ArrayList<Routines>) q.list();
            sesija.getTransaction().commit();
            sesija.close();

            cb_chose_workout.getItems().clear();
            cb_user_workouts.getItems().clear();
            for (Routines s : routines1) {
                cb_chose_workout.getItems().add(s.getRoutineName());
                cb_user_workouts.getItems().add(s.getRoutineName());
            }
        }

        //PROGRESS TRACKER ------------------------------------------------------------------------------------------
        if (event.getSource()
                == btn_progress_tracker) {
            pnl_progress_tracker.toFront();
            acp_progress_tracker.toFront();
            acp_workout.toBack();
            acp_workouts.toBack();
            acp_exercises.toBack();
        }
        //CURRENT WORKOUT -----------------------------------------------------------------------------------------

        if (event.getSource()
                == btn_current_workout) {
            pnl_current_workout.toFront();
            acp_progress_tracker.toBack();
            acp_workout.toFront();
            acp_workouts.toBack();
            acp_exercises.toBack();

        }

        //All WORKOUTS TAB -----------------------------------------------------------------------------------------
        if (event.getSource()
                == btn_all_workouts) {
            pnl_all_workouts.toFront();
            acp_progress_tracker.toBack();
            acp_workout.toBack();
            acp_workouts.toFront();
            acp_exercises.toBack();
        }
        //ALL EXERCISES ------------------------------------------------------------------------------------------

        if (event.getSource()
                == btn_all_exercises) {
            pnl_all_exercises.toFront();
            acp_progress_tracker.toBack();
            acp_workout.toBack();
            acp_workouts.toBack();
            acp_exercises.toFront();
        }
        //POGRESAN UNOS ---------------------------------------------------------------------------------------------

        if (event.getSource()
                == btn_empty_field) {
            pnl_registration.toFront();
        }

        if (event.getSource()
                == btn_empty_field1) {
            pnl_registration.toFront();
        }
        //LOG OUT ----------------------------------------------------------------------------------------------------

        if (event.getSource()
                == btn_log_out) {
            pnl_logged_in.toBack();
            pnl_admin.toBack();
            pnl_log_reg.toFront();
            pnl_registration.toFront();
        }

    }

    public void mouseActionHandler(MouseEvent event) {
        //EXIT -------------------------------------------------------------------------------------------------------

    }

    //INITALIZE ||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||\
    @Override
    public void initialize(URL url, ResourceBundle rb) {

        //Postavljanje workout choice box-a 
        ArrayList<Routines> routines;
        Session sesija = HibernateUtil.getSessionFactory().openSession();
        sesija.beginTransaction();
        Query q = sesija.createQuery("from Routines");
        routines = (ArrayList<Routines>) q.list();
        sesija.getTransaction().commit();
        sesija.close();

        cb_workouts.getItems().clear();
        for (Routines s : routines) {
            cb_workouts.getItems().add(s.getRoutineName());
        }
        //Postavljanje workout choice box-a 
        ArrayList<Routines> routines1;
        sesija = HibernateUtil.getSessionFactory().openSession();
        sesija.beginTransaction();
        q = sesija.createQuery("from Routines");
        routines1 = (ArrayList<Routines>) q.list();
        sesija.getTransaction().commit();
        sesija.close();

        cb_chose_workout.getItems().clear();
        cb_user_workouts.getItems().clear();
        for (Routines s : routines1) {
            cb_chose_workout.getItems().add(s.getRoutineName());
            cb_user_workouts.getItems().add(s.getRoutineName());
        }

        //Postavljanje exercise choice box-a ----------------------------------------------------------------
        ArrayList<Exercises> exercises;
        sesija = HibernateUtil.getSessionFactory().openSession();
        sesija.beginTransaction();
        q = sesija.createQuery("from Exercises");
        exercises = (ArrayList<Exercises>) q.list();
        sesija.getTransaction().commit();
        sesija.close();

        cb_exercises.getItems().clear();
        cb_user_exercises.getItems().clear();
        for (Exercises s : exercises) {
            cb_exercises.getItems().add(s.getExerciseName());
            cb_user_exercises.getItems().add(s.getExerciseName());
        }
        //Postavljanje Users choice box-a -----------------------------------------------------------------------
        ArrayList<Users> users;
        sesija = HibernateUtil.getSessionFactory().openSession();
        sesija.beginTransaction();
        q = sesija.createQuery("from Users");
        users = (ArrayList<Users>) q.list();
        sesija.getTransaction().commit();
        sesija.close();

        cb_admin_users.getItems().clear();
        for (Users s : users) {
            cb_admin_users.getItems().add(s.getUsername());
        }

        cb_users.getItems().clear();
        for (Users s : users) {
            cb_users.getItems().add(s.getUsername());
        }

        //Postavljanje Diff Lvl choice boxova
        ArrayList<DifficultyLevel> diff_lvl;
        sesija = HibernateUtil.getSessionFactory().openSession();
        sesija.beginTransaction();
        q = sesija.createQuery("from DifficultyLevel");
        diff_lvl = (ArrayList<DifficultyLevel>) q.list();

        sesija.getTransaction().commit();
        sesija.close();

        cb_diff.getItems().clear();
        cb_diff_exe.getItems().clear();
        cb_diff_work.getItems().clear();

        for (DifficultyLevel s : diff_lvl) {
            cb_diff.getItems().add(s.getLevelsOfDifficulty());
            cb_diff_exe.getItems().add(s.getLevelsOfDifficulty());
            cb_diff_work.getItems().add(s.getLevelsOfDifficulty());
        }
        //Postavljanje MUSCLES choise boxova
        ArrayList<Muscles> muscles;
        sesija = HibernateUtil.getSessionFactory().openSession();
        sesija.beginTransaction();
        q = sesija.createQuery("from Muscles");
        muscles = (ArrayList<Muscles>) q.list();

        sesija.getTransaction().commit();
        sesija.close();

        cb_muscles.getItems().clear();
        cb_exe_muscles.getItems().clear();

        for (Muscles s : muscles) {
            cb_muscles.getItems().add(s.getMuscleName());
            cb_exe_muscles.getItems().add(s.getMuscleName());
        }

        //PODESAVANJE POCETNOG PROZORA -------------------------------------------------------------------------------------------------
        pnl_log_reg.toFront();
        pnl_admin.toBack();

    }

}
