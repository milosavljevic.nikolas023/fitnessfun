-- MySQL dump 10.13  Distrib 5.7.24, for Win64 (x86_64)
--
-- Host: localhost    Database: mydb
-- ------------------------------------------------------
-- Server version	5.7.24-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `difficulty_level`
--

DROP TABLE IF EXISTS `difficulty_level`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `difficulty_level` (
  `iddifficulty_level` int(11) NOT NULL AUTO_INCREMENT,
  `levels_of_difficulty` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`iddifficulty_level`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `difficulty_level`
--

LOCK TABLES `difficulty_level` WRITE;
/*!40000 ALTER TABLE `difficulty_level` DISABLE KEYS */;
INSERT INTO `difficulty_level` VALUES (1,'Easy'),(2,'Normal'),(3,'Hard');
/*!40000 ALTER TABLE `difficulty_level` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `exercises`
--

DROP TABLE IF EXISTS `exercises`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `exercises` (
  `idExercises` int(11) NOT NULL AUTO_INCREMENT,
  `exercise_name` varchar(45) NOT NULL,
  `exercise_description` varchar(45) DEFAULT NULL,
  `Users_idUsers1` int(11) NOT NULL,
  `difficulty_level_iddifficulty_level` int(11) NOT NULL,
  PRIMARY KEY (`idExercises`),
  KEY `fk_Exercises_Users1_idx` (`Users_idUsers1`),
  KEY `fk_exercises_difficulty_level1_idx` (`difficulty_level_iddifficulty_level`),
  CONSTRAINT `fk_Exercises_Users1` FOREIGN KEY (`Users_idUsers1`) REFERENCES `users` (`idUsers`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_exercises_difficulty_level1` FOREIGN KEY (`difficulty_level_iddifficulty_level`) REFERENCES `difficulty_level` (`iddifficulty_level`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `exercises`
--

LOCK TABLES `exercises` WRITE;
/*!40000 ALTER TABLE `exercises` DISABLE KEYS */;
INSERT INTO `exercises` VALUES (1,'Push up','Osnovna vezba za gornji deo tela',2,1);
/*!40000 ALTER TABLE `exercises` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `exercises_has_muscles`
--

DROP TABLE IF EXISTS `exercises_has_muscles`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `exercises_has_muscles` (
  `Exercises_idExercises` int(11) NOT NULL,
  `Muscles_idMusclesParts` int(11) NOT NULL,
  KEY `fk_Exercises_has_Muscles_Muscles1_idx` (`Muscles_idMusclesParts`),
  KEY `fk_Exercises_has_Muscles_Exercises1_idx` (`Exercises_idExercises`),
  CONSTRAINT `fk_Exercises_has_Muscles_Exercises1` FOREIGN KEY (`Exercises_idExercises`) REFERENCES `exercises` (`idExercises`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_Exercises_has_Muscles_Muscles1` FOREIGN KEY (`Muscles_idMusclesParts`) REFERENCES `muscles` (`idMusclesParts`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `exercises_has_muscles`
--

LOCK TABLES `exercises_has_muscles` WRITE;
/*!40000 ALTER TABLE `exercises_has_muscles` DISABLE KEYS */;
INSERT INTO `exercises_has_muscles` VALUES (1,6);
/*!40000 ALTER TABLE `exercises_has_muscles` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `muscles`
--

DROP TABLE IF EXISTS `muscles`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `muscles` (
  `idMusclesParts` int(11) NOT NULL AUTO_INCREMENT,
  `muscle_name` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`idMusclesParts`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `muscles`
--

LOCK TABLES `muscles` WRITE;
/*!40000 ALTER TABLE `muscles` DISABLE KEYS */;
INSERT INTO `muscles` VALUES (1,'Biceps'),(2,'Triceps'),(3,'Ledja'),(4,'Noge'),(5,'Ramena'),(6,'Grudi');
/*!40000 ALTER TABLE `muscles` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `progress_tracker`
--

DROP TABLE IF EXISTS `progress_tracker`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `progress_tracker` (
  `idStats` int(11) NOT NULL AUTO_INCREMENT,
  `weight` int(11) DEFAULT NULL,
  `exercise_reps_tracker` int(11) DEFAULT NULL,
  `date` datetime DEFAULT NULL,
  `Users_idUsers` int(11) NOT NULL,
  PRIMARY KEY (`idStats`),
  KEY `fk_Progress_Tracker_Users1_idx` (`Users_idUsers`),
  CONSTRAINT `fk_Progress_Tracker_Users1` FOREIGN KEY (`Users_idUsers`) REFERENCES `users` (`idUsers`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `progress_tracker`
--

LOCK TABLES `progress_tracker` WRITE;
/*!40000 ALTER TABLE `progress_tracker` DISABLE KEYS */;
/*!40000 ALTER TABLE `progress_tracker` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `routines`
--

DROP TABLE IF EXISTS `routines`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `routines` (
  `idRoutines` int(11) NOT NULL AUTO_INCREMENT,
  `routine_name` varchar(45) NOT NULL,
  `routine_goal` varchar(45) DEFAULT NULL,
  `routine_description` varchar(400) DEFAULT NULL,
  `difficulty_level_iddifficulty_level` int(11) NOT NULL,
  PRIMARY KEY (`idRoutines`),
  KEY `fk_routines_difficulty_level1_idx` (`difficulty_level_iddifficulty_level`),
  CONSTRAINT `fk_routines_difficulty_level1` FOREIGN KEY (`difficulty_level_iddifficulty_level`) REFERENCES `difficulty_level` (`iddifficulty_level`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `routines`
--

LOCK TABLES `routines` WRITE;
/*!40000 ALTER TABLE `routines` DISABLE KEYS */;
INSERT INTO `routines` VALUES (1,'Push - Pull','Dodavanje snage i misica','Ova rutina je mogo dobra',2);
/*!40000 ALTER TABLE `routines` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users` (
  `idUsers` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) NOT NULL,
  `surname` varchar(45) NOT NULL,
  `username` varchar(45) NOT NULL,
  `password` varchar(45) NOT NULL,
  `sex` tinyint(4) NOT NULL,
  `height` int(11) DEFAULT NULL,
  PRIMARY KEY (`idUsers`),
  UNIQUE KEY `username_UNIQUE` (`username`),
  UNIQUE KEY `UK_r43af9ap4edm43mmtq01oddj6` (`username`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES (2,'admin','admin','admin','admin',0,185);
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users_has_routines`
--

DROP TABLE IF EXISTS `users_has_routines`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users_has_routines` (
  `Users_idUsers` int(11) NOT NULL AUTO_INCREMENT,
  `Routines_idRoutines` int(11) NOT NULL,
  PRIMARY KEY (`Users_idUsers`),
  KEY `fk_Users_has_Routines_Routines1_idx` (`Routines_idRoutines`),
  KEY `fk_Users_has_Routines_Users1_idx` (`Users_idUsers`),
  CONSTRAINT `fk_Users_has_Routines_Routines1` FOREIGN KEY (`Routines_idRoutines`) REFERENCES `routines` (`idRoutines`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_Users_has_Routines_Users1` FOREIGN KEY (`Users_idUsers`) REFERENCES `users` (`idUsers`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users_has_routines`
--

LOCK TABLES `users_has_routines` WRITE;
/*!40000 ALTER TABLE `users_has_routines` DISABLE KEYS */;
/*!40000 ALTER TABLE `users_has_routines` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2019-01-16  2:25:34
